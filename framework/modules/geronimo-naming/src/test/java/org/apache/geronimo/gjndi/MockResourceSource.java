/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */


package org.apache.geronimo.gjndi;

import org.apache.geronimo.naming.ResourceSource;
import org.apache.geronimo.gjndi.binding.MockDataSource;
import org.apache.geronimo.gbean.annotation.GBean;

/**
 * @version $Rev: 905918 $ $Date: 2010-02-03 15:26:50 +0800 (Wed, 03 Feb 2010) $
 */
@GBean
public class MockResourceSource implements ResourceSource {
    public Object $getResource() throws Throwable {
        return new MockDataSource();
    }
}
