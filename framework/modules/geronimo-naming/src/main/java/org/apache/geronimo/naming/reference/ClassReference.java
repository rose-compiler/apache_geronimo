/**
 *  Licensed to the Apache Software Foundation (ASF) under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.apache.geronimo.naming.reference;

import javax.naming.NamingException;

/**
 * @version $Rev: 933374 $ $Date: 2010-04-13 03:24:39 +0800 (Tue, 13 Apr 2010) $
 */
public class ClassReference extends SimpleAwareReference {

    private String className;
    
    public ClassReference(String className) {
        this.className = className;
    }

    @Override
    public Object getContent() throws NamingException {
        ClassLoader loader = getClassLoader();
        try {
            return loader.loadClass(className);
        } catch (ClassNotFoundException e) {
            NamingException ex = new NamingException("Unable to load class " + className);
            ex.initCause(e);
            throw ex;
        }
    }    
        
}
